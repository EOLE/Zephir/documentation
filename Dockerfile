FROM python:alpine

MAINTAINER eole <eole@ac-dijon.fr>

RUN apk upgrade --update-cache --no-cache
RUN apk add curl make unzip git

# sphinx-doc install

# Install mscgen
RUN cd '/tmp' && \
    curl -fsSL "http://www.mcternan.me.uk/mscgen/software/mscgen-static-0.20.tar.gz" -o '/tmp/mscgen.tar.gz' && \
    tar -zxvf '/tmp/mscgen.tar.gz' && \
    mv /tmp/mscgen-0.20/bin/mscgen /usr/bin/ && \
	rm -rf /tmp/mscgen-*

#RUN apt install python3-sphinx mscgen python3-jinja2 python3-yaml plantuml
RUN apk add py3-pip
RUN pip install -U sphinx
RUN pip install -U pyyaml

# Install plantuml ??
#RUN apk --update add openjdk7-jre graphviz ttf-droid ttf-droid-nonlatin
#RUN mkdir /app && \
#    curl -fsSL https://sourceforge.net/projects/plantuml/files/plantuml.1.2019.0.jar/download -o /app/plantuml.jar \
#    echo "#/bin/sh java -jar /app/plantuml.jar $@" > /usr/bin/plantuml && \
#    chmod +x /usr/bin/plantuml

# Install jinja2 et yaml
RUN apk add py3-jinja2 py3-yaml

# Install sphinxcontrib-mscgen
RUN cd /tmp && \
    curl -fsSL "https://github.com/alfred82santa/sphinxcontrib-mscgen/archive/master.zip" -o '/tmp/sphinxcontrib-mscgen.zip' && \
    unzip '/tmp/sphinxcontrib-mscgen.zip' && \
    cd /tmp/sphinxcontrib-mscgen-master && \
    sed -i -e 's/from sphinx.util.compat import Directive/from docutils.parsers.rst import Directive/' ./sphinxcontrib/mscgen.py && \
    python setup.py install && \
    rm -rf '/tmp/sphinxcontrib-mscgen.tar.gz'

RUN pip install -U sphinxcontrib-plantuml
#RUN pip3 install -U sphinxcontrib-mscgen
#RUN pip3 install -U sphinxcontrib-plantuml
# FIXME hack: mscgen-contrib is **not** python3 compatible
#COPY scripts/mscgen.py /usr/local/lib/python3.6/dist-packages/sphinxcontrib/

# html generation
ADD DocumentationTechnique /DocumentationTechnique
WORKDIR /DocumentationTechnique

# Common python Zephir library
ARG PYTHON_ZEPHIR_REPO_URL=https://gitlab.mim.ovh/EOLE/Zephir/python-zephir.git
ARG PYTHON_ZEPHIR_REPO_REF=0.1.0
RUN git clone "${PYTHON_ZEPHIR_REPO_URL}" "/tmp/python-zephir" \
    && cd /tmp/python-zephir \
    && git checkout "${PYTHON_ZEPHIR_REPO_REF}" && \
    cd /DocumentationTechnique && \
    mv /tmp/python-zephir/zephir ./_scripts

# API messages description
ARG MESSAGES_API_REPO_URL=https://gitlab.mim.ovh/EOLE/Zephir/messages-api.git
ARG MESSAGES_API_REPO_REF=0.1.0
RUN git clone "${MESSAGES_API_REPO_URL}" "/tmp/messages-api" \
    && cd /tmp/messages-api \
    && git checkout "${MESSAGES_API_REPO_REF}" &&\
    mkdir -p /services/common && \
    mv /tmp/messages-api/messages /services/common/

RUN make html

# just to access to the generated doc on the host
RUN cp -a /DocumentationTechnique/_build/html/ /vol


WORKDIR /DocumentationTechnique/_build/html
CMD python -m http.server 3333
EXPOSE 3333

