Déployer un environnement Zéphir en 5 minutes
=============================================

Présentation du projet Zéphir
------------------------------

:Nom du projet: `Zéphir <https://gitlab.mim.ovh/EOLE/Zephir>`_
:URL du projet: https://gitlab.mim.ovh/EOLE/Zephir
:version: alpha
:Démo: https://lab14-eole.ac-dijon.fr

Prérequis
-----------

- 4 Go de RAM minimum

- Installer l'application sur un système Linux Ubuntu (>= 18.04)

- Installer Docker et Docker-compose avec les paquets Ubuntu

  .. code-block:: bash

      # apt install docker.io docker-compose

  Vérifier que Docker fonctionne à l'aide de la commande suivante

  .. code-block:: bash

      # service docker status

- Installer les outils Docker à la main
  
  - installer Docker

  `Docker <https://docs.docker.com/engine/installation/>`_

  Dans ce cas, installer la version Docker `community edition <https://docs.docker.com/install/linux/docker-ce/ubuntu/#docker-ee-customers>`_

  - installer Docker-compose

  `Docker-Compose <https://docs.docker.com/compose/install/>`_

  S'assurer de disposer d'un nom de domaine valide et accessible OU renseigner le ficher `/etc/hosts` de la machine qui permet d'accéder à Zéphir

  .. code-block:: bash

    # vim /etc/hosts
    127.0.0.1  <nom-de.domaine>

Installer Git
--------------

Git est un logiciel de gestion de versions décentralisé.

.. code-block:: bash

    $ apt install git

Récupérer l'application Zéphir
-------------------------------

Cloner le dépôt Git nommé `application
<https://gitlab.mim.ovh/EOLE/Zephir/application.git>`_ :

.. code-block:: bash

    $ mkdir zephir && cd zephir
    $ git clone https://gitlab.mim.ovh/EOLE/Zephir/application.git

Se placer dans le répertoire `./application/` du dépôt Git :

.. code-block:: bash

    $ cd ./application/

.. warning:: Toutes les commandes qui seront exécutées ensuite le seront depuis ce répertoire.

Se placer sur la branche de la version en cours de développement :

.. code-block:: bash

    $ git checkout develop

Configurer l'infrastructure `Zéphir`
------------------------------------

Créer le fichier de configuration `zephir.config` à partir du modèle `zephir.config.sample`, éditer le fichier de configuration, configurer l'environnement (nom de domaine et identifiants des différents comptes d'administration) :

.. code-block:: bash

    $ cp zephir.config.sample zephir.config
    $ vim zephir.config

.. code-block:: bash

    DEBUG=1
      
    # Zephir
    ZEPHIR_DOMAIN=ubuntuserver.ac-test.fr
    ZEPHIR_END_POINT=https://ubuntuserver.ac-test.fr
    SSO_END_POINT=https://ubuntuserver.ac-test.fr
    
    #SSO (keycloak)
    KEYCLOAK_USER=admin
    KEYCLOAK_PASSWORD=admin
    KC_REALM_USERNAME=admin_zephir
    KC_REALM_PASSWORD=admin_zephir
    
    # Saltmaster-manager
    SALTMASTER_PUBLIC_HOST=ubuntuserver.ac-test.fr


Démarrer l’infrastructure Zéphir
---------------------------------

L'utilitaire `zephir-compose` enrobe la commande `docker-compose` et en facilite l’utilisation.

Démarrer l’infrastructure, actuellement il s'agit de l'infrastructure de développement :

.. code-block:: bash
    
    $ cd ./compose/

.. code-block:: bash

    $ ./zephir-compose --base dev -- up -d

La commande permet de télécharger les images Docker depuis la plateforme Docker : https://hub.docker.com/u/eoleteam.

Les arguments `docker-compose` sont placés après le séparateur `--`.

L'option `-d` permet de lancer l'application en arrière plan.

.. warning:: Une fois que la commande rend la main il faut attendre un peu avant que l'interface s'affiche.

Découvrir l'application Zéphir
------------------------------

Ouvrir un navigateur et saisir l'URL déclarée dans le fichier `zephir.config`.

Se connecter avec les identifiants déclarés dans ce même fichier.

Arrêter l'infrastructure Zéphir
--------------------------------

Arrêter les conteneurs :

.. code-block:: bash

    $ ./zephir-compose --base dev -- down

Erreur dans la configuration de l'infrastructure
-------------------------------------------------

Une erreur dans la configuration, notamment dans la saisie du nom de domaine, oblige à supprimer les volumes docker.

.. warning:: Toutes les données seront perdues.

Arrêter les conteneurs en suppriment les volumes docker :

.. code-block:: bash

    $ ./zephir-compose --base dev -- down -v

Relancer l'infrastructure Zéphir :
  
.. code-block:: bash

    $ ./zephir-compose --base dev -- up -d

.. warning:: Une fois que la commande rend la main il faut attendre un peu avant que l'interface s'affiche.
