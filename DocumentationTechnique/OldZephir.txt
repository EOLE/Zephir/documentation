Liste des fonctionnalités présentes dans Zéphir 'V1'
====================================================

Fonctionnalités "métiers" constituant l'ancienne application Zéphir.

Etablissements::
  - Gestion des types d'établissement (education ?)

Modules/Variantes::
  - Définition des paquets livrés par défaut avec chaque module (paquets incluant des dictionnaires seulement)
  - Possibilité de gérer des modules non livrés par Eole

Personnalisation de variantes/serveurs::
  - Possibilité de spécifier des dictionnaires, templates, patchs, valeurs de configuration et fichiers/paquets supplémentaires

Gestion de la configuration (modules/variantes/serveurs)::
  - Gestion d'un pool de dictionnaire Creole par version de distribution/paquet
  - Détection des dictionnaires installés manuellement sur les clients

Gestion des autorisations::
  - Définitions de groupes de droits basés sur les fonctions autorisées dans l'API
  - Ajout de restrictions sur les ressources (non croisé avec les groupes de droit) de type : serveur/groupe/etablissement/module/variante

Fonctionnalité diverses::
  - Gestion centralisée d'un pool d'identifiants (distribution d'identifiants ENT aux clients)
  - Distribution de certificats (utilisé pour rsyslog, non maintenu)
  - Synchronisation des fichiers d'import AAF
  - Gestion de la réplication LDAP Scribe/Seshat (maj de la conf ldap seshat depuis scribe)

Exécution à distance::
  - Envoi de la clé ssh sur les serveurs (droits d'administration requis -> pas pratique)

Gestion de l'état des serveurs/surveillance::
  - Vérification de l'état de mise à jour des serveurs (maintien d'un cache des paquets dispos sur les serveurs de maj -> à remplacer par un check sur les clients ?)
  - somme md5 des fichiers 'critiques' des serveurs (config.eol, patchs, ...)
