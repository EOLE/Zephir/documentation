Variables d'environnement
=========================

Liste des variables
-------------------

- DEBUG : Défini le mode DEBUG pour le développement 
    - type : bool, ex: 1
- ZEPHIR_DOMAIN : Nom de domaine du service Zephir 
    - type : nom de domaine, ex : zephir2.ac-test.fr
- ZEPHIR_END_POINT : Url du service Zephir 
    - type : nom de domaine, ex : https://zephir2.ac-test.fr
- SALTMASTER_PUBLIC_HOST : Nom de domain du service saltmaster
    - type : nom de domaine, ex : zephir2.ac-test.fr
    - note : ce service est interne à Zephir, mais pourra etre externalisé
- SSO_END_POINT : Url du serveur SSO
    - type : nom de domaine, ex : https://zephir2.ac-test.fr
    - note : un service keycloak interne a zephir est utilisé par défaut.
- KEYCLOAK_USER : Nom d'utilisateur Administrateur du service Keycloak
- KEYCLOAK_PASSWORD : Mot de passe Administrateur du service Keycloak
- KC_REALM_USERNAME : Nom d'utilisateur par défaut
- KC_REALM_PASSWORD : Mot de passe par défaut

