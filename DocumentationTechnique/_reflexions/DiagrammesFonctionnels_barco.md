Diagrammes fonctionnels pour Zéphir
Modifier cette section
Point de vue administration
Modifier cette section
Définition non exhaustive d'une liste d'éléments

    Utilisateurs
    Permissions
    Structures administratives
    Réseaux
    Serveurs
    Services
    Paramètres de configuration
    ...

Modifier cette section
Tous les éléments sont administrables

    Ajout/suppression/modification d'éléments

Modifier cette section
liens entre les éléments

    utilisateur <--> permission
    permission <--> réseau
    serveur <--> service
    serveur <--> réseau
    permission -> serveur
    structure adminisrative <--> réseau
    structure adminisrative <--> paramètre de configuration
    ...

Modifier cette section
Groupes d'éléments

    Tous les serveurs du même type
    Toutes les structures administratives du même type
    ...

Modifier cette section
Liens entre les groupes d'éléments

    groupe de permissions -> groupe de serveurs
    groupe de services -> groupe de serveurs
    ...

Modifier cette section
Les éléments peuvent hériter d'autres éléments

    Surcharge de paramètres de configuration
    paramètre de configuration lié à une structure -> serveur
    ...

Modifier cette section
Point de vue fonctionnel

    Un utilisateur doit pouvoir connecter avec un compte local
    Un utilisateur doit pouvoir connecter avec un compte distant
    Un utilisateur doit pouvoir gérer un groupe d'éléments restreints
    Un utilisateur doit pouvoir avoir les droits root sur un serveur
    Un utilisateur doit pouvoir déployer une configuration sur un serveur
    Un utilisateur doit pouvoir déployer une infrastructure dans une structure administrative
