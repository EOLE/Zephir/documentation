Diagrammes fonctionnels pour Zéphir
Modifier cette section
Caractéristiques générales

    rôle de suivi des alertes
    de mauvais fonctionnement
    listing et gestion des actions
        opérations de maintenance sur les serveurs,
        paramétrages de services
        actions de migration
        exécution de commande personnalisée (typiquement ce qui serait fait "habituellement" en ssh)
    contacts avec le serveurs
    différents types de vues doivent être disponibles (groupes de serveurs, établissements/zones, cartographique...)
    remontée et descentes par lots (cas de politique de sécurité de pare-feu, descente de modèles XML Era)

Modifier cette section
Fonctionnements à changer / améliorer

    La procédure d'enregistrement zéphir se fera avec salt ainsi que tout contact avec le serveur
    décoreller les processus d'authentifications, de profils -> une appli à part
    le traitement par lot (groupe de serveur, etc.. ) est à améliorer
        critères de sélections de serveurs plus fins
        tags sur certains serveurs
    conserver une notion de module, variantes, etc..
    la gestion de la configuration est de la responsabilité du zephir
    aujourd'hui, des possibilités d'action sur le serveur dérivent par rapport au module/variante,
        il faudrait formaliser cela (notion d'héritage de variantes)
        notamment la "Liste des fichiers personnalisés" qui, s'ils sont personnalisés depuis le Zéphir, provoquent de fait des différences par rapport au module de base (ou de la variante)
        les dictionnaires additionnels impliqueront de créer une nouvelle variante de fait
        tout template, patch, etc... additionnels
