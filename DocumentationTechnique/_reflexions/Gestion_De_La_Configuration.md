Groupe de travail Gestion de la configuration
Modifier cette section
Participants

    Joël
    William
    Lionel
    Gwen
    Barco
    Laurent
    Manu
    Daniel
    Philippe
    Gilles
    Benjamin
    Thierry

Modifier cette section
Fonctionnalités

    Ontologie, lien sémantique (ex: "Quel est le DNS à utiliser pour telle structure ?")
    Gestion des dépendances entre les variables
    Contextualisation : établissement, zone, ... (possibilité d'héritage)
    Gestion clé/valeur (la valeur pouvant être un hash)
    Plusieurs valeur par défaut proposées (avec des tags)
    Valeur par défaut obtenues par ontologie
    Historisation (versionning avec commentaires) et cycle de vie des configurations (prévoir un workflow pour les configurations de dév, de pré-production, ...)
    Planification de la mise en place d'une nouvelle configuration stable (manuelle ou automatisée, exemple : la nuit prochaine)

Modifier cette section
Plan d'action

Référents:

    Plan A: Daniel / William
    Plan B: Laurent / Barco
    Plan C: Manu/ Joël

============================================================================================    
Plan A

Architecture logicielle

Voir le schema d’une architecture logicielle basée sur les ontologies ontology-arch.pdf.
Modifier cette section
Service « Référentiel »

    Objectifs
        Abstraire l’accès à la base référentiel pour ne pas être lié à une technologie particulière
        Valider les entrées/sorties de la base
            Les requêtes utilisent-elles bien un des vocabulaires déclarés ?
            Les valeurs sont-elles bien du type voulu ?
    Supporte un système de hooks permettant d’étendre facilement les validations et manipulation des ontologies
    Interrogeable par une API basée sur les ontologies et la manipulation de graphes

Modifier cette section
Service « ACL »

    Enregistrer de nouvelles ACLs
    Consulter les ACLs
    Déterminer si une entité a un droit X sur une autre entité, cela permet aux autres services de filtrer les résultats de requête

Modifier cette section
Service « Suggestions »

    Fournir une API simplifiée (REST) d’accès aux informations, à-là creoled

Modifier cette section
Service « Orchestrateur »

    Fournir à l’orchestrateur les informations issues du référentiel
    Déclanche des actions sur l’orchestrateur en fonction d’événements sur le référentiel

Modifier cette section
L’API Gateway

    Permet d’unifier la vue API des services

Modifier cette section
Frontend

Que du classique…
Modifier cette section
Principes des ontologies

La gestion par ontologies permet de stocker des liens sémantiques afin de répondre à des question comme :

    Quelle est l’adresse du service proxy de mon réseau ?
    Quels sont les services qui utilisent l’annuaire LDAP dont l’adresse IPv4 est XXX.XXX.XXX.XXX ?

Modifier cette section
Définition des liens sémantiques

La définition de lien sémantique peut se faire avec des N-triple ou N-Quad :

    Un sujet
    Un prédicat (qui peut être vu comme un verbe)
    Un objet
    Un label optionel dans le cas des N-Quad

Modifier cette section
Exemple d’une académie de test

Voici l’interprétation de certaines relations présentes dans le fichier de tests N-Quad-académie-de-test.txt :

    Une académie est une zone

    <aca_test>     <type>         "zone"         .

    Une académie contient des serveurs

    # Server 1 in academie
    <aca_test>     <contains>     <server_1>     .
    <server_1>     <type>         "server"     .
    # Server 2 in academie
    <aca_test>     <contains>     <server_2>     .
    <server_2>     <type>         "server"     .

    Une académie contient des établissements

    <aca_test>     <contains>     <etb_1>     .
    <etb_1>         <type>         "zone"         .

    Un serveur héberge des services

    <server_1>     <hosts>     <dns_1>     .
    <dns_1>         <type>         "service"     .
    <dns_1>         <provides>     "DNS::Server"     .

Modifier cette section
Interrogation du graphe des liens

Il est possible d’interroger la base de données orienté graphe avec différents protocoles :

    Gremlin
    Gizmo (a Gremlin like language)
    Metaweb Query Language

Pour mon serveur « server_4 », quels sont les entités « services » qui fournissent le « rôle » « HTTP::Proxy »


var serveur4 = g.V("<server_4>")
var zonePath = g.M().In('<contains>')
var serverPath = g.M().Out('<contains>').Has('<type>', 'server')
var httpProxyServicePath = g.M().Out('<hosts>').Has('<type>', 'service').Has('<provides>', 'HTTP::Proxy')

var zones = serveur4.FollowRecursive(zonePath)

zones.ForEach(function(z) {
  g.V(z.id).FollowRecursive(serverPath).ForEach(function(s) {
    g.V(s.id).FollowRecursive(httpProxyServicePath).All()
  })
});

Modifier cette section
Résumé
Modifier cette section
Point forts

    Architecture très prometteuse car extrèmement souple et évolutive
    Une approche par contexte et non par contrainte
    Un développement de zéro ⮕ liberté d’approche architecturale

Modifier cette section
À ne pas oublier

    Gros travail sur le vocabulaire à définir
    L’ontologie ne répond pas à tous les problèmes, il faudra donc développer des services
    La solution qui a le plus d’inconnues

Modifier cette section
Webographie

    Ontology based architecture
    Ontology-based Service Discovery Front-end Interface for GloServ
    Evolving graphs and pokemon
    Time-Based versioned graphs
    Suggested Upper Merged Ontology défini des ontologies pour l’informatique
    Cours SPARQL Protocol and RDF Query Language sur Wikiversity

============================================================================================    
Plan B : transformation - modularisation

Modifier cette section
Objectifs

    Réutiliser au maximum l'existant
    Séparation des composants
    Génération de la configuration sur une machine centrale (ou plusieurs)
        hiérarchiser la configuration
            un niveau zone (contexte général de l'infrastructure)
            un niveau service affecté à une zone
    Sauvegarde de la configuration dans une base distribuée type Redis
        réplication
        interrogation optimisée (index)
    Gestion des templates
        Déploiement des templates par SALT

Modifier cette section
Travail à réaliser

    Séparer la gestion des variables de zone (globales) des variables de services (héritent d'une zone)
    Adapter Gen_config
        dictionnaire de zone (mandatory non multi) : édition d'une seule zone à la fois
        dictionnaire de service attaché à une zone et un nom d'hôte
        Dump configuration dans la base Redis
    Bind des configurations (templates et dictionnaires + /etc/eole) pour utiliser Gen_config
        Chargement de la configuration existante depuis un export de la base vers un config.eol
    Utilisation des templates cheetah par SALT
        un nouveau renderer pour SALT qui exploite les variables de la base Redis (il semble que cela ne soit pas possible mais on peut utiliser CreoleCat depuis SALT d'ores et déjà)
        les templates pourront migrer progressivement vers Jinja

Modifier cette section
Problèmes à résoudre

    Affichage de la listes des services à activer dans une zone
    Importer une zone pour en créer une nouvelle
    Héritage de zone
        Une multi de zones mères
        zones mères non modifiables

============================================================================================    
Plan C

Architecture proposée
    Module
    Variante
        force don't change value
        force default
        force default if same
        only config
    Serveur
Réponse aux exigences


Adaptation de Créole pour utiliser les mécanismes de gestion des variantes, des groupes et des serveurs implémentés dans Tiramisu.
Modifier cette section
Architecture proposée
Modifier cette section
Module

Un module est un ensemble de services/dictionnaires.
EOLE fournit des modules.
Si un service est ajouté/modifié, il s'agit d'un nouveau module qui peut être créé à partir d'un module existant (EOLE ou non).
Si on crée un module à partir d'un module existant, les personnalisations (non EOLE) peuvent être soit copiées soit héritées (modification de structure dynamique).
Chaque module définit donc une structure de configuration fixe.
Chaque module fournit un XML décrivant la totalité de la structure (actuellement le fichier "flattened") qui est le point de départ de tout chargement de configuration.

Le niveau module est entièrement géré au niveau Creole.
Modifier cette section
Variante

La variante permet de modifier les valeurs par défaut d'un module ou d'une autre variante (héritage de variante).
Il est possible de créer une variante à partir d'une variante existante. Dans ce cas, les modifications apportées sur la variante parente seront héritées dynamiquement.

Dans le cas où l'on modifie une variante, la répercussion de cette modification sur les variantes filles et les configurations serveurs est paramétrable :
Modifier cette section
force don't change value

Seule la valeur de la variante est modifiée, mais les valeurs finales des configurations filles non. Pour obtenir ce résultat, les valeurs default sont remplacées par des valeurs personnalisées.
Cela ressemble au mode de fonctionnement qu'on avait sur l'ancien Zéphir.
Seules les nouvelles configurations filles héritent de la nouvelle valeur.

Si la valeur est forcée à "non" dans la variante, on obtient le résultat suivant :
avant   default     "oui"   "non"
après   "oui"   "oui"   "non"
Modifier cette section
force default

La valeur est réinitialisée à "default" sur toutes les configuration filles, toute personnalisation est perdue.

Quelque soit la valeur saisie dans la variante, on obtient le résultat suivant :
avant   default     "oui"   "non"
après   default     default     default
Modifier cette section
force default if same

Si la valeur personnalisée du serveur est la même que celle configurée dans la variante, alors elle est réinitialisée à default.

Si la valeur est forcée à "non" dans la variante, on obtient le résultat suivant :
avant   default     "oui"   "non"
après   default     "oui"   default
Modifier cette section
only config

Force une valeur personnalisée sur les serveurs héritant de la variante sans modifier celle-ci ni les variantes intermédiaires. Cela ressemble à la fonctionnalité "appliquer une valeur sur un groupe de serveur" de l'ancien Zéphir avec un groupe correspondant aux serveurs héritant de la variante.

Si la valeur est forcée à "non", on obtient le résultat suivant :
avant   default     "oui"   "non"
après   "non"   "non"   "non"

FIXME : Le comportement en cas d'héritage de variante est encore à déterminer pour certains des cas décrits ci-dessus.

Le niveau variante est principalement géré par Tiramisu (objet MetaConfig).
Modifier cette section
Serveur

Il est toujours possible de surcharger les valeurs par défaut héritées des différentes variantes ou du module au niveau serveur.

Le niveau serveur est principalement géré par Tiramisu (objet Config).
Modifier cette section
Réponse aux exigences

    propriétés des variables : déjà géré (dictionnaires Creole)
    contextualisation : non implémenté mais faisable
        ajout d'attributs sur les variables
        implémenter le pré-remplissage des variables à partir des informations de la structure/de la zone
        implémenter un système d'ontologie plus poussé (coût à évaluer)
    Plusieurs valeurs proposées (suggestions) :
        existe déjà grâce au "valid_enum", voir comment étendre/trier la liste à partir des informations du contexte
        évolution à prévoir : pouvoir afficher un commentaire (tag, remarque, propriété, ...) à côté de chacune des proposition dans l'interface
    Gestion clé/valeur : à implémenter mais cela ne semble pas très complexe
        possibilité d'exporter la configuration sous la forme clé/valeur (dans le goût de CreoleGet --list) dans une base de données ou autre forme de stockage
        + utilisation de ces données intermédiaires pour les requêtes simples (les valeurs finales ne sont pas recalculées)
        + possibilité de récupérer la dernière configuration connue d'un serveur même en cas de défaillance du service
        + possibilité de mise à jour partielle en cas d'évolution des configurations ou du contexte
        + la partie utilisant ce service n'est pas à recoder en cas d'ajout d'une nouvelle source de données de configuration
        + possibilité de décider du moment auquel une configuration préparée est exportée
    Historisation des configurations : à développer
    Cycle de vie des configurations
        Proposition : pouvoir avoir plusieurs configurations par variante dans l'interface avec des noms fixes (dév, ...) ou personnalisés avec possibilité de copier une configuration sur une autre (ex : dév -> stable). Techniquement chaque configuration est une nouvelle variante (objet MetaConfig) mais cela peut tout à fait être masqué à l'utilisateur ;)
    Scalabilité
        possibilité d'associer les variantes et les serveurs à des nœuds particuliers
    Zéphir établissement
        gestion des valeurs des serveurs de l'établissement sur un nœud particulier
    API
        microservices avec une API dédiée à développer (possibilité de réutiliser celle de "genconfig")

    
