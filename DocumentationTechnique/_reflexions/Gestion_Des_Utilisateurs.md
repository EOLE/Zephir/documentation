Groupe de travail Gestion des utilisateurs

h2. Participants

    Joël
    William
    Manu
    Daniel
    Philippe
    Luc
    Benjamin
    Gérald
    Emmanuel Ihry

Modifier cette section
Plan d'action

Référents:

    Plan A: Benjamin / William

Modifier cette section
Plan A: étude de solution d'API Gateway + compatibilité avec le modèle d'ACL prévu
Modifier cette section
Notes de la réunion

    Authentification externalisée: OpenID Connect, SAMLv2, CAS, OAuth2
    Pas d'annuaire local
    Service de gestion de comptes simple local (BDD)
    Matrice de droits à deux entrées: Action -> Entité
    Gestion par rôle avec héritage de droits
    Délégation de droits (1 niveau) partielle et totale avec période de validité
    Attributs des profils mutivalués avec identification de la source + tag "J'utilise cette source"
    Gestion de la notion d'équipe de rattachement sur les profils

========================================================================================    
    
Gestion des utilisateurs Zéphir
Modifier cette section
Plan A

Dans le schéma actuellement retenu, l’accès aux fonctionnalités de l’application Zéphir repose sur une passerelle traitant les requêtes d’un utilisateur et une base de données regroupant et organisant les informations sur les permissions de chaque utilisateur.
Modifier cette section
API Gateway
Modifier cette section
Quel est le rôle de la passerelle ?

Le rôle identifié de la passerelle est d’intercepter les requêtes vers les différents services constituant l’application Zéphir et de les faire aboutir ou non sur la base des permissions publiées par un autre microservice.
Modifier cette section
Comment la passerelle interagit avec le client et les services ?

Les services sont envisagés comme des programmes proposant une API ou des applications web (utilisation du protocole HTTP).
La passerelle devrait donc être capable d’intercepter les requêtes et, éventuellement, de les modifier ou de forger une réponse.

Une question en suspens : comment restreindre l’accès aux différents services au passage par la passerelle ? Écoute des serveurs en local uniquement ?

La passerelle est responsable de l’authentification et est capable d’interroger la matrice de permission avec un identifiant d’utilisateur et un identifiant de fournisseur d’identité.
Modifier cette section
Matrice de permissions

Les permissions permettent d’associer un utilisateur à une action dans un contexte, avec une durée de validité.
Le contexte comme l’action doivent avoir une correspondance avec les requêtes utilisées pour y accéder.

                    autorisation
utilisateur --------------------------> (contexte, action)
            avec condition de validité

La matrice de permissions est consultée à l’aide d’une API et n’est pas responsable de l’authentification.

Le cas de la délégation devrait être traité avec les mêmes outils : la délégation nécessite une permission spécifique, elle est opérée via un service avec une API, derrière la passerelle. Dans ce cas, l’action est « délégation » et le contexte, la liste des tuples (contexte, action) que l’utilisateur a le droit de déléguer.
Modifier cette section
Conclusion de l’étude

La conclusion importante est la disponibilité de solutions répondant aux besoins exprimés. La modularité pour le découpage en microservices est une contrainte qui reste à évaluer dans le cas du choix d’une solution complète.
La gestion des permissions profiterait de l’adoption d’un modèle plus flexible que RBAC. La gestion par rôle est contraignante quand il s’agit de mettre en place une granularité fine. XACML propose un cadre pour la mise en place d’une politique de gestion des permissions basée sur les attributs.

Les deux solutions complètes qui sortent du lot sont LemonLDAP::NG et WSO2 (en deux briques, Identity Server et API Manager).

L’avantage de partir sur une solution complète, outre que ça dispense de développements importants, est l’utilisation d’un flux déjà bien implémenté entre les différents services.

Dans le cas où on part sur une infrastructure plus éclatée (en termes de solutions mise en œuvre, pas de nombre de services ou microservices mis en place), se pose la question de la communication et du rôle de chaque brique. La passerelle doit-elle être centrale et servir de pivot pour le traitement des requêtes ? Le serveur d’identité doit-il être central et faire le travail de traitement pour ne renvoyer que les résultats à la passerelle ?
Modifier cette section
Suite de l’étude¶

Le deuxième temps de l’étude devrait permettre de statuer sur le type d’infrastructure (solution complète ou agrégat de briques indépendantes).

La troisième partie de l’étude devrait permettre de disposer d’un prototype avec la solution retenue.

