# -*- coding: utf-8 -*-
import glob, jinja2, sys
from os.path import join, exists, basename, dirname, realpath
from os import makedirs

rootpath = realpath(dirname(realpath(__file__)) + '/../../')
sys.path.insert(0, join(rootpath, 'services', 'common', 'python'))

from zephir.message import message_definition

print("Generating Zephir's API csv from YAML sources...")

message_root_path = join(rootpath, 'services', 'common', 'messages')
#version_folders = glob.glob(join(message_root_path, '*/'))
messages_doc_path = join(rootpath, 'DocumentationTechnique', 'messages')
message_definition.MESSAGE_ROOT_PATH = message_root_path

env = jinja2.Environment(loader=jinja2.FileSystemLoader(messages_doc_path, encoding='utf-8'))
api_template = env.get_template('api.csv.j2')

api = []
for uri in message_definition.list_messages():
    message_def = message_definition.get_message(uri)
    api.append(message_def)

# On génère le fichier d'index de l'API dans la version courante
dest_path = join(messages_doc_path, 'api.csv')
new_content = api_template.render(api=api).encode('utf-8')

with open(dest_path, 'w') as dest_file:
    print("generating messages index...")
    dest_file.write(new_content.decode())
