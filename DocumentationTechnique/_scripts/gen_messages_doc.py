# -*- coding: utf-8 -*-
import glob, jinja2, sys
from os.path import join, exists, basename, dirname, realpath
from os import makedirs

rootpath = realpath(dirname(realpath(__file__)) + '/../../')
sys.path.insert(0, join(rootpath, 'services', 'common', 'python'))

from zephir.message import message_definition

print( "generating Zephir's messages documentation from YAML sources...")

message_root_path = join(rootpath, 'services', 'common', 'messages')
version_folders = glob.glob(join(message_root_path, '*/'))
messages_doc_path = join(rootpath, 'DocumentationTechnique', 'messages')
message_definition.MESSAGE_ROOT_PATH = message_root_path

env = jinja2.Environment(loader=jinja2.FileSystemLoader(messages_doc_path, encoding='utf-8'))
message_template = env.get_template('message.txt.j2')
index_template = env.get_template('index.txt.j2')

# On parcourt chaque dossier de version des messages
api = {}
for uri in message_definition.list_messages():

    message_def = message_definition.get_message(uri)

    dest_path = join(messages_doc_path, message_def.version, message_def.uri + '.txt')

    if not exists(dirname(dest_path)):
        makedirs(dirname(dest_path))

    new_content = message_template.render(msg=message_def).encode('utf-8')

    with open(dest_path, 'w') as dest_file:
        print("message '{:s}' changed. generating sphinx documentation...".format(message_def.uri))
        dest_file.write(new_content.decode())

    api.setdefault(message_def.version, [])
    api[message_def.version].append(message_def)

for version, messages in api.items():
    # On génère le fichier d'index de l'API dans la version courante
    dest_path = join(messages_doc_path, version, 'index.txt')
    new_content = index_template.render(version=version, api=messages).encode('utf-8')

    if exists(dest_path):
        with open(dest_path, 'r') as dest_file:
            if dest_file.read() == new_content:
                continue

    with open(dest_path, 'w') as dest_file:
        print("generating messages index...")
        dest_file.write(new_content.decode())
