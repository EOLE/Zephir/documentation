Epic 10 : Définition et implémentation des actions Zéphir
=========================================================

E10-1: Identifier les différentes actions disponibles sur un serveur Zéphir
---------------------------------------------------------------------------

Contexte
~~~~~~~~

L'application Zéphir doit permettre d'effectuer un certains nombres d'"actions" sur
le Zéphir. Ces actions se matérialisent parfois par la simple exécution d'une commande
sur le serveur, parfois par des procédures plus complexes (exemple: "appliquer la configuration").

Proposition
~~~~~~~~~~~

Faire un état des lieux des actions possibles sur le Zéphir premier du nom et celles attendues par les utilisateurs.

Identifier les actions maintenues sur le nouveau Zéphir.

Définir les scénarios d'implémentation des actions retenues.

Critères d'acceptation
~~~~~~~~~~~~~~~~~~~~~~

* Les actions Zéphir à implémenter sont définies
* Les scénarios d'implémentation sont rédigés et évalués pour chacune des actions retenues
