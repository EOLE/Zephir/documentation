Epic 11 : Gestion des services applicatifs
==========================================

E11-1: Gérer les dépendances entre services applicatifs
-------------------------------------------------------

Contexte
~~~~~~~~

Les modèles de serveur racine dépendent uniquement de services applicatifs
génériques (exemple : eole-server, eole-amon, eole-scribe, ...).
Ces services applicatifs doivent eux même dépendre des services applicatifs
qui constituent le module (eole-proxy, eole-annuaire, ...)

L'ajout d'un service applicatif sur un modèle de serveur personnalisé doit
entraîner l'ajout des services applicatifs dont il dépend (exemple : eole-web
pour eole-roundcube).

L'affaire peut se corser si l'on souhaite gérer le choix (exemple : apache ou
nginx) et les conflits entre services applicatifs.


Proposition
~~~~~~~~~~~

Identifier les services applicatifs existants et leurs relations.

Proposer une solution pour gérer les dépendances entre services applicatifs.

Critères d'acceptation
~~~~~~~~~~~~~~~~~~~~~~

 * implémenter les dépendances pour "eole-roundcube"

E11-2 : Créer la base datasource des services applicatifs
---------------------------------------------------------

Contexte
~~~~~~~~

La description des version, source et servermodel existe dans la datasource.
Il faut ajouter les serviceApplicatifs, Document, Package

Proposition
~~~~~~~~~~~

* Utiliser le zephir 2.6.2 comme base pour créer les listes.
* la procédure de mise à jour du datasource va être très proche ==> idée : à scripter ?

Critères d'acceptation
~~~~~~~~~~~~~~~~~~~~~~

* le datasource est complet.

E11-3 : Mettre a disposition les sources de données
-----------------------------------------------------

Contexte
~~~~~~~~

Il faut disposer d'un serveur de données SGBD et d'un serveur de données de
stockage de fichiers.

Proposition
~~~~~~~~~~~~

* Définir les accès aux bases de données de type SGBD
* Proposer un service de données de type fichier

Critères d'acceptation
~~~~~~~~~~~~~~~~~~~~~~

Un micro-service a accès à la base de données et peut déposer/récupérer des fichiers
dans le stockage de fichiers.

E11-4 : Peuplement des sources de données à partir du dataset
-------------------------------------------------------------

Contexte
~~~~~~~~

Les données du dataset doivent être chargées dans les différentes source de
données (postgres, minio, ...)

Proposition
~~~~~~~~~~~~

* Mettre à disposition l'ensemble des sources de données
* Le serveur data utilise le serveur controleur pour mettre à jour les sources
  de données

Critères d'acceptation
~~~~~~~~~~~~~~~~~~~~~~

Des sources de données sont peuplées à partir du dataset.

E11-5 : CRUD sur les services applicatifs et les sous-entités
-------------------------------------------------------------

Contexte
~~~~~~~~

On fait la distinction entre sources automatiques, apportées par l’application elle-même, et sources utilisateurs, éditées dans l’application.

On doit être capable de créer, lister, supprimer, modifier les données des services applicatifs hors initialisation et selon les sources.
Par exemple, pour les sources automatiques (EOLE, Envole), on ne peut faire que la lecture. Pour les sources utilisateurs, l’ensemble des actions CRUD doit être possible.


Proposition
~~~~~~~~~~~

* Ajouter les fonctions pour la création d’une source
* Déterminer un mécanisme permettant de définir les droits sur les opérations
* Ajouter les fonctions pour les actions CRUD sur les services applicatifs prenant en compte l’aspect des droits en fonction des sources et de l’agent.

Critères d’acceptation
~~~~~~~~~~~~~~~~~~~~~~

L’utilisateur peut créer une source de données et des services applicatifs associés.
L’utilisateur peut modifier, supprimer, lister les services applicatifs de la source qu’il a créée 
L’utilisateur peut lister les services applicatifs d’une autre source.
