Epic 6 : Gestion des serveurs (clos)
====================================

E6-1 : Implémentation d'un service de gestion des entités "Server"
--------------------------------------------------------------------

Contexte
~~~~~~~~

L'application Zéphir doit pouvoir manipuler des références vers un certain nombre de "serveurs" (voir entité :ref:`serverdomain.server`), hôtes physiques ou virtuels hébergeant des services applicatifs.
Chacun de ces serveurs doit être attaché à un modèle de serveur (voir entité ServerModel).

Proposition
~~~~~~~~~~~

Implémenter un service dont le rôle sera la gestion et la persistance des entités "Server" dans l'application Zéphir.

Voir

- :ref:`server.delete`
- :ref:`server.find`
- :ref:`server.update`

Critères d'acceptation
~~~~~~~~~~~~~~~~~~~~~~

* Un message transmis sur le bus de messages permet d'interroger le service concernant les serveurs enregistrés
* Un message transmis sur le bus de messages permet d'enregistrer un nouveau serveur

  * Un identifiant sera automatiquement attribué à ce serveur
  * Ce serveur devra être attaché à un modèle de serveur à sa création

* Un message transmis sur le bus de messages permet de supprimer un serveur existant
* Pour chacune des opérations modifiant l'état d'un serveur, un événement correspondant est envoyé sur le bus de messages

E6-2 : Liaison entre un "minion" Saltstack et un "Server" Zéphir
------------------------------------------------------------------

Contexte
~~~~~~~~

L'application Zéphir devrait avoir un lien logique entre les instances de :ref:`server` et les machines ("minions") appairées avec le processus `salt-master`.

Il existe plusieurs cinématiques possibles :

* le serveur génère la clé : https://docs.saltstack.com/en/latest/topics/tutorials/walkthrough.html#using-salt-key
* le Zéphir génère la clé : https://docs.saltstack.com/en/latest/topics/tutorials/preseed_key.html

Proposition
~~~~~~~~~~~

(proposition retenue : le Zéphir génère la clé)
***********************************************

1. Sur le Zéphir, l'utilisateur créait un nouveau :ref:`server` sur Zéphir (avec le bon :ref:`server-model` associé)
2. Le service `saltmaster` réagit à la création de ce nouveau :ref:`server` et créait un paire de clés cryptographiques à destination du minion. La paire de clé est dédiée/liée au :ref:`server`.
3. L'utilisateur récupère cette paire de clés et la déploie manuellement sur la machine avec le minion

Une partie du traitement pourrait être automatisée via un script (comme c'est le cas dans le Zéphir actuel). Cette solution permet de garantir que la personne qui appaire le serveur à vraiment le droit de le faire mais nécessite un ensemble d'action non automatisé par Salt.


.. mscgen::

  msc {
      hscale = "2";

      saltminion,zephir_connect,server_manager,salt_master,saltmaster;

      zephir_connect=>zephir_connect [ label = "key generation" ];
      zephir_connect=>server_manager [ label = "ask for available server list" ];
      zephir_connect<<server_manager [ label = "return available server list" ];
      zephir_connect=>salt_master [ label = "ask salt UID" ];
      salt_master>>zephir_connect [ label = "return salt UID" ];
      zephir_connect=>zephir_connect [ label = "set minion configuration" ];
      zephir_connect=>saltminion [ label = "start daemon" ];
      saltminion=>saltmaster [ label = "saltminion to saltmaster connection" ];
      saltmaster->* [ label = "signal new key" ];
      salt_master=>salt_master [ label = "verify new key" ];
      salt_master=>saltmaster [ label = "issue key validation command" ];
      saltmaster->* [ label = "key validated" ];
      salt_master=>server_manager [ label = "associate salt UID with server ID" ];
      server_manager->* [ label = "salt UID associated with server ID" ];
      }


Schéma cinématique de fonctionnement de l'appairage d'un serveur
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Depuis l'UI (interface graphique utilisateur) jusqu'au dépôt de la
configuration d'apérage dans vault (le coffre fort).


.. mscgen::

  msc {
    hscale = "2";

    user,server_manager,postgres, salt_master, vault;

    user => server_manager [label="create server"];
    server_manager => postgres[label="insert server"];
    postgres => server_manager[label="server id"];
    server_manager => user[label="server id"];
    server_manager => vault[label="put passphrase"];
    server_manager => salt_master[label="server created"];
    vault => salt_master[label="get passphrase"];
    salt_master => vault[label="put peer configuration"];
    user => server_manager[label="get peer configuration"];
    server_manager => vault [label="get peer configuration"];
    vault => server_manager [label="peer configuration"];
    server_manager => user[label="peer configuration"];
  }


Voir également
**************

- :ref:`salt.minion.unregistered`
- :ref:`salt.minion.registered`

Critères d'acceptation
~~~~~~~~~~~~~~~~~~~~~~

* On peut appairer un :ref:`server` sur l'application Zéphir et faire le lien entre un "minion" et un :ref:`server`.

E6-3 : Exécution de commandes sur les serveurs enregistrés
-------------------------------------------------------------

Contexte
~~~~~~~~

L'application Zéphir devrait permettre d'exécuter une commande sur un serveur en utilisant la clé "Zéphir" de ce serveur et non la clé "Salt" du minion.

Proposition
~~~~~~~~~~~

Implémenter un mécanisme (service ?) permettant de transformer un message d'exécution de commande utilisant la clé d'un serveur en message d'exécution Salt.

Voir

- :ref:`exec.salt`


Critères d'acception
~~~~~~~~~~~~~~~~~~~~

* Un nouveau message est défini, permettant de lancer l'exécution d'une commande sur un serveur donné.
* Ce message est automatiquement "traduit" en message d'exécution de commande "v1.salt.exec" si et seulement si le serveur est effectivement géré par Salt.
