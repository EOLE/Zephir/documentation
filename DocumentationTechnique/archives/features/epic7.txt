Epic 7 : Gestion des autorisations
==================================

  TODO Ajouter/Lier le CR du groupe de travail "Zéphir" du hackathon 2017

E7-1 : Identifier le modèle d'autorisation de l'application Zéphir
---------------------------------------------------------------------

Contexte
~~~~~~~~

Un certains nombres d'entités vont être manipulées dans l'application Zéphir.

Le MTES a exprimé un certain nombre de besoins indiquant qu'une gestion des droits
"fins" (i.e. qui s'appliquent sur la valeur de certains attributs d'entité et
non pas sur l'entité dans son ensemble) sera nécessaire.

La possibilité d'appliquer une politique d'autorisation sur un groupe
(dynamique ou non) d'entités a également été exprimé.

Proposition
~~~~~~~~~~~

Sélectionner un panel de scénarios d'utilisation de l'application mettant en lumière
les mécanismes d'autorisation.

Faire un état des lieux des différentes stratégies de modélisation de politique
d'autorisation (RBAC, ABAC, etc...).

Statuer sur la stratégie à adopter pour le nouveau Zéphir en fonction des besoins
exprimés par les utilisateurs et des contraintes métiers/techniques de l'application.

Garder en mémoire la possibilité d'appliquer un requête d'autorisation sur un groupe
d'entité (statique ou dynamique).

Critères d'acceptation
~~~~~~~~~~~~~~~~~~~~~~

- Une stratégie pour la modélisation de la politique d'autorisation a été choisie et
  le choix répond aux besoins exprimés par les utilisateurs et les scénarios d'utilisation
  sélectionnés.

E7-2 : Sélectionner/implémenter le socle du modèle d'autorisation de l'application Zéphir
----------------------------------------------------------------------------------------------

.. important:: Cette proposition de scénario doit être affinée après la réalisation du scénario E7-1.

Contexte
~~~~~~~~

Afin de pouvoir être utilisé par tous les microservices, le système de gestion des autorisations
devra lui même être disponible sous la forme d'un microservice.

Proposition
~~~~~~~~~~~

Identifier une solution existante (service, librairie...) permettant d'implémenter le modèle
d'autorisation retenu et l'intégrer à l'application Zéphir.

Pistes de recherche
*******************

- https://github.com/casbin/ - Librairie/serveur d'autorisation multi-modèle

Critères d'acceptation
~~~~~~~~~~~~~~~~~~~~~~

- Un message permet d'interroger le service d'autorisation concernant les droits d'accès d'un utilisateur (UID ?)
  et une entité (ou groupe) donnée (ou un attribut d'une entité ?).
- Un message permet d'assigner un droit sur une entité (ou groupe) (ou un de ses attributs ?) à un utilisateur (UID ?)
- Un message permet de supprimer un droit (mêmes conditions que précédemment)...
