Epic 8 : Gestion des sites et des zones
==========================================

E8-1 : Créer le microservice en charge de la gestion des zones
----------------------------------------------------------------

Contexte
~~~~~~~~

Les entités :ref:`server` doivent pouvoir être rattachées à une ou plusieurs :ref:`zone`.
Cela permettra d'appliquer des filtres de sélection et des règles d'autorisation d'accès en fonction de sa ou ses :ref:`zone` de référence.

Proposition
~~~~~~~~~~~

Implémenter un microservice (`zone-manager` ?) capable d'effectuer les opérations CRUD sur une entité :ref:`zone` et d'y attacher/détacher un :ref:`server`.

Critères d'acceptation
~~~~~~~~~~~~~~~~~~~~~~

* Le microservice est implémenté et permet d'effectuer les principales opérations CRUD sur l'entité :ref:`zone`
* Le microservice permet d'attacher/détacher un :ref:`server` à une :ref:`zone`
* Des messages sont envoyés sur le service `crossbar` pour chaque changement d'état d'une :ref:`zone`
* Le microservice peut fonctionner avec un `scale > 1`

E8-2 : Créer le microservice en charge de la gestion des sites
-----------------------------------------------------------------

Contexte
~~~~~~~~

Les entités :ref:`zone` doivent pouvoir être rattachées à un :ref:`site`. Cela permettra
d'appliquer des filtres de sélection des :ref:`server` et des règles d'autorisation d'accès en fonction de son :ref:`site` de référence hérité de ses :ref:`zone`.

Proposition
~~~~~~~~~~~

Implémenter un microservice (`site-manager` ?) capable d'effectuer les opérations CRUD sur une entité :ref:`site` et d'y attacher/détacher une :ref:`zone`.

Critères d'acceptation
~~~~~~~~~~~~~~~~~~~~~~

* Le microservice est implémenté et permet d'effectuer les principales opérations CRUD sur l'entité :ref:`site`
* Le microservice permet d'attacher/détacher une :ref:`zone` à un :ref:`site`
* Des messages sont envoyés sur le service `crossbar` pour chaque changement d'état d'un :ref:`site`
* Le microservice peut fonctionner avec un `scale > 1`

E8-3 : Évaluer la possibilité de créer automatiquement les zones après une session d'édition de configuration d'un serveur
-----------------------------------------------------------------------------------------------------------------------------

Contexte
~~~~~~~~

À l'issue d'une session d'édition de la configuration d'un :ref:`Server`, on pourra inférer potentiellement une :term:`zone` de
rattachement de base. Il faudrait évaluer la possibilité de créer automatiquement (au besoin) l'entité :ref:`zone` correspondante
et attacher le :ref:`server` à celle ci.

Proposition
~~~~~~~~~~~

  TODO

Critères d'acceptation
~~~~~~~~~~~~~~~~~~~~~~

  TODO

Notes
-----

Idée : gérer dynamiquement la création des sites et des zones lors de l'enregistrement des paramètre de configuration
paradigme : comme des préférences

+ dans les profils : une identité est associée à un site (ou plus ? )
  ==> cela permettra de créer des acls du type : "Serveur faisant parti du mes sites"

+ on peut filtrer les serveurs avec un critère "site"
+ question a ce poser : est ce que le site est mémorisé dans le "server" ?

