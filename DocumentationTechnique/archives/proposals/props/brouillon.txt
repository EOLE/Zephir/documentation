Mode brouillon
================

Contexte
---------

On veut pouvoir enregistrer une configuration sans avoir renseigné toutes les variables obligatoires.

Proposition 1
---------------

Rajouter un bouton "Enregistrer comme brouillon" dans le menu de l'interface `gen_config`. 
Dans ce cas, la validation des variables obligatoires ne sera pas effectuée. 

Modifications nécessaires : 

- ajouter un bouton à l'interface `gen_config`
- ajouter une nouvelle API pour enregistrer sans valider les variables obligatoires
- la configuration doit être marquée avec le tag `brouillon`
- le tag `brouillon` doit empêcher l'envoi du message `v1.config.configuration.server.saved`


