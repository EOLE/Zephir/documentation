Methodologies de développement
==============================

:date: 2018-10-12

Situation actuelle
--------------------

Celui qui développe ne merge pas dans dévelop. 
Nécessité d'un code review. 

Dans la pratique, le code review a été abandonné depuis l'EAD3.

L'idée de faire le code review en fin de sprint n'est pas bonne. 

Il faudrait le faire **dès que possible**

Idée : à la mèlée, mettre une colonne supplémentaire correspondant à la différence 
entre *fini* et *résolu*.

La revue de code
----------------

- revenir à une revue de code 
- traiter une revue de code dans la journée, dès qu'elle est signalée à la mèlée
- au sortir de la mèlée, celui qui a accepté la revue de code et l'auteur du code
  travaillent en binôme sur le *pull request*. A noter que **c'est celui qui a accepté le merge
  qui a la main**.

Pratiques à définir sur l'API
-----------------------------

- nécessité de versionner l'API, dès que ça "casse" l'existant  
- le versionnement des API : 

    - soit l'API est versionnée
    - soit il y a une version par messages
  
  Il faut une unicité entre l'API et les messages. 
  L'API v2 doit appeler des messages v2 (et pas encore des messages de v1)
  Sinon c'est une erreur de design.

- Soit, à l'intérieur d'une application, l'appli peut prendre des messages 
  en v1 et d'autres en v2.
- Soit, on clone les messages de v1, ils appartiennent à v2, le service n'utilise
  **que** des messages v2.

.. important:: on choisit le fait de forker pour le passage à v2 (même si ça fait 
               globalement la même chose).


La base de données
-------------------

Il est nécessaire de concevoir la base de données comme **locale**, 
et derrière les databases sont *réunifiées* en backend. 
Mais chaque microservice dialogue avec sa base de données à lui.

L'asynchrone dans les microservices
-------------------------------------

L'exemple d'amazon : l'analyse sur la fonction/valeur prime sur l'analyse technique. 
Remise en cause de la loi d'intégrité fonctionnelle par rapport à la base de données.
Cette intégrité fonctionnelle peut être cassée car ce qui a le plus de valeur est le panier utilisateur.
En plus, une liaison forte entre le microservice qui gère les dépôts et le microservice qui gère 
les commandes ne prend pas en compte le fait que le dépôt peut être réapprovisionné entre temps, 
et que la commande du client va *probablement* être honorée...

Etant bien entendu que la rupture de cohérence dans la base de donnée induit de la complexité.

.. note:: Toujours se poser la question : est-ce que le coût est justifié par la valeur ?

Le choix des outils
--------------------

- Sur la partie `git`, nous n'avons pas de besoins supplémentaires.
- Sur la partie d'intégration continue, il y a nécessité de passer à un outil plus adapté.

- les IDE utilisés doivent être compatibles avec la plateforme utilisée
  (possibilité de lancer les tests unitaires et fonctionnels, etc...)




