Sprint 2018 41-43
==================

:date: 8 octobre 2018


`Demandes PO`_

.. _`Demandes PO`: Demandes.html



prod

  Ce qui sera destiné aux utilisateur  

pre-prod

  La future stable. C'est là qu'on valide que les exigences utilisateur 
  sont bien remplies.

Développement
-----------------

planning en fonction de l'importance des demandes

#. Login/logout

  - rendre l'API-Bridge compatible avec JWT
  - supprimer les utilisateurs `yo` et `admin`

#. Seed

  - créer un utilisateur par script
  - créer un serveur

#. Création de serveur -> **OK**
#. Appairage : message d'erreur à créer
#. Configuration : 

    - mode brouillon
    - MAJ des données d'environnement
    
#. Déploiement     

    - envoyer tous les fichiers
    - créer des utilisateurs "SSH"

#. suppression d'un serveur

Pour la suite
--------------

Variante de Servermodel
~~~~~~~~~~~~~~~~~~~~~~~~~

#. ServeurModel en lecture 
#. ServeurModel en écriture 
#. ServeurModel migration
#. ServeurModel variantes
#. Ajout d'un ServeurModel

- Site/Zone
- Groupes de serveurs
- Stats
- EAD3
- Journaux : mise en place d'un logTash et d'un kibana
- Migration zéphir 1 vers Zéphir 2
- Variante de configuration



