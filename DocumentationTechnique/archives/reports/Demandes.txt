Demandes PO Zéphir
==================

.. contents::

:date: 8 octobre 2018

Refaire un tour d'architecture
--------------------------------

- kong 
- keycloak
- crossbar...

- et les mettre à jour (pas de versions figées en dev)
- voir s'il faut les conserver ou les changer (kong? -> revalider tout)

- faire un bilan des flux plutôt que du schéma d'architecture lui-même.
  (quel message est envoyé vers qui, etc...)

- le design est trop technique, il devrait être plus orienté métier.

- re-spécifier les API. Les transformer en message. 
  peu importe le microservice récepteur/émetteur du message.  

- identifier clairement ce que fait l'API/ les messages :

  exemple : la liste des serveurs : est-ce que ce sont les serveurs en totalité,
  ou bien les serveurs sur lesquels la personne authentifiée a les droits ? 

- décoreller les droits utilisateurs/accès de la question du retour des API.
  reprendre l'API, la valider, **être sûr du mapping entre l'API et les messages**.
  
Re-spécifier notre API
----------------------

reconstruire l'édifice des micro-services, les re-valider un par un.

- faut-il une api-bridge ? 

- exemple : exécuter des commandes sur tous les scribe

- le microservice qui redémarre les Scribe fait la demande de la liste des scribes.

- système de gestion des droits : rien n'est encore fait.

.. important:: Ne plus différer la question de la gestion des droits/actions.
               Mettre en priorité la question du sytème de gestion de droits


Au minimum, on a besoin : 

- du login de l'utilisateur
- d'un register de session

Remplacer la notion de session par un micro-service dédié

- Reste-t-il des points en suspens ? (cf l'effet démo qui n'est pas une démo)
- Aboutir à quelque chose qui soit *fini* (au sens utilisateur).

Les parties métier
--------------------

- Tester avec le `zephir-client`
- Règler les questions d'authentification
- Écrire les tests fonctionnels les uns derrière les autres
- Faire en sorte que le scripting soit opérationnel
- **L'UI est importante mais elle n'est pas urgente**,
  faire avancer les deux en parallèle (la partie UI et la partie ligne de commande)
- la partie salt
- la partie client (ui)

- faire fonctionner le client ligne de commande (zephir client)

- bilan de l'UI

    - retirer le fake code
    - authentifications/ gestion des utilisateurs
    - vision claire de ce que fait l'ui

L'UI est *presque* un développement indépendant. Elle pourrait être développée 
par une équipe externe. 

- l'url d'accès est versionnée (`/v1/...`)
- l'UI doit être versionnée 
- les technologies de l'UI peuvent éventuellement être changées (react/redux)
  si nécessaire.
- faut-il reprendre l'existant ou bien repartir from scratch ? 
  (qu'est-ce qui est le plus rapide?)
- faut-il une formation, mixer les technos, etc... ? 

Changement de forge
---------------------

- passage progressif d'un code nettoyé sur du **gitLab**
- abandon de `git-flow` pour des méthodologies mieux adaptées à l'intégration continue
- fonctionner par la méthode du pull-request (quelqu'un accepte les commits des autres dev)
- des groupes de branches qui, suivant les scénarios et les épiques, sont mergées **avant** d'être
  placées dans la branche `develop`
  

.. important:: L'objectif est de faire un pipe-line le plus auto-validant possible 
               de manière à sortir le produit le plus rapidement possible.

