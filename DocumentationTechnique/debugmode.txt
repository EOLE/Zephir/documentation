Les outils de debug
====================

Le mode `DEBUG`
-----------------

En mode `DEV`, c'est à dire avec l'option `-b dev`,  

.. code-block:: bash

    zephir-compose -b dev -- up --build -d 


on active le mode `DEBUG` en renseignant la variable d'environnement 

.. code-block:: bash

    DEBUG=1

Dans le fichier :file:`compose/dev/zephir.env`.

Ainsi on peut voir tous les évenements qui se sont déroulés dans les différents conteneurs.

Accès aux logs des événements 
------------------------------

En mode `DEBUG`, la liste des événements (les échanges de message au niveau du bus de messages) 
apparaît dans les logs. 

Les logs sont accessibles via la commande

.. code-block:: bash

    zephir-compose -b dev -- logs -f <nom_du_conteneur>

Exemple 

.. code-block:: bash

    zephir-compose -b dev -- logs -f servermodel-manager
