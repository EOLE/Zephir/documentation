ExecutionDomain
===============

Description
-----------

Service gérant l'exécution à distance de procédures sur les serveurs

Mise à jour
~~~~~~~~~~~

Les mises à jour sont gérées directement par le Zéphir. L'utilisateur n'aura plus la possibilité d'utiliser des commandes EOLE pour changer les dépôts, les versions, ... directement depuis le serveur.

Entités
-------

Messages
--------
