ServerDomain
============

Description
-----------

Service gérant les serveurs enegistrés dans l'application Zéphir

- enregistrement des serveurs
- recherche/sélection d'ensembles de serveurs suivant des critères
- affichage de l'état des serveurs

Entités
-------

- `Server <../entities/server.html>`_
- `ServerSelection <../entities/server-selection.html>`_
- `UO <../entities/organizational-unit.html>`_

Messages
--------
