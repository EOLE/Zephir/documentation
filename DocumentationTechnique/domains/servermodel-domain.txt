ServermodelDomain
=================

Description
-----------

Service gérant la description des modèles de serveurs.

Un modèle de serveur est un ensemble de services applicatifs.

Service applicatif
~~~~~~~~~~~~~~~~~~

Un service applicatif contient l'ensemble des informations utiles a la construction d'un serveur :

- dictionnaire Creole
- templates de configuration
- documents divers (fichiers, patchs)
- paquets supplémentaires à installer
- dépendances
- ...

Les services applicatifs ne sont pas modifiables par l'utilisateur.

Modèle de serveur
~~~~~~~~~~~~~~~~~

Un modèle de serveur contient une liste de services applicatifs associés.

Un modèle de serveur peut hériter d'un autre modèle de serveur. Il hérite alors de ses associations.
L'utilisateur peut créer de nouveaux modèles de serveur et ajouter/supprimer des associations sur ses modèles de serveur.

Source
~~~~~~

Les services applicatifs sont fournis par deux types de source :

- un conteneur datasource (EOLE, Envole, ...)
- l'importation pré-créée dans l'ancien Zéphir

Le conteneur datasource est construit à partir de paquets qui comprennent l'ensemble des informations.
L'installation de ce paquet s'effectuant depuis un mirroir ou cache disponible sur le serveur. Un utilisateur aura la possibilité de bloquer les mises à jour du dépôt.

Release et version
~~~~~~~~~~~~~~~~~~

Les services applicatifs et les modèles de serveurs sont associés à une release, elle-même associé à une version d'une distribution particulière.

Les services applicatifs et les modèles de serveur sont versionnés dans la base. Chaque version d'un service applicatif est associé à une sous-release, elle même associée à une release particulière.

L'utilisateur ne connait pas l'existance des sous-release.

Modèle de serveur et Serveur
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Le modèle de serveur fournit l'ensemble des documents qui seront utilisés sur
le serveur. Même si ces documents sont fournis par un paquet, c'est la version provenant du modèle serveur qui sera utilisée.

Les paquets ou personnalisations installés directement sur le serveur ne seront plus pris en compte.

Diagrammes de séquence
----------------------

Import d’une source de données
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. mscgen::

  msc {
      hscale = "2";
      Datasource,Consul,ServermodelData,Postgres;
      Datasource=>Consul [ label = "s'annonce" ];
      Consul=>ServermodelData [ label = "déclenche le script d'importation" ];
      ServermodelData=>Datasource [ label = "télécharge le nouveau SHA256SUM" ];
      ServermodelData=>Postgres [ label = "liste les SHA256SUM" ];
      ServermodelData=>ServermodelData [ label = "SHA256SUM dans liste ?" ];
      --- [ label = "le nouveau SHA256SUM n'est pas dans la liste : changement dans le datasource" ];
      ServermodelData=>Datasource [ label = "demande la description des services applicatifs" ];
      Datasource=>ServermodelData [ label = "envoie la description des services applicatifs" ];
      ServermodelData=>Postgres [ label = "release si nécessaire" ];
      ServermodelData=>Postgres [ label = "nouvelle sous-release" ];
      --- [ label = "Pour chaque service applicatif" ];
      ServermodelData=>Postgres [ label = "créé service applicatif" ];
      --- [ label = "Pour chaque document" ];
      ServermodelData=>Datasource [ label = "télécharge le document" ];
      ServermodelData=>Postgres [ label = "insère le document" ];
  }
..
  .. uml::
    :scale: 80 %
    :align: center

    actor Admin
    database Datasource
    control Consul
    entity ServermodelData
    database Postgres

    Admin -> Datasource: démarre
    Consul -> Datasource: détecte
    Consul -> ServermodelData: annonce
    group Importation des données
        ServermodelData -> Datasource: demande le SHA256SUM global
        Datasource -> ServermodelData: envoie le SHA256SUM global
        ServermodelData -> Postgres: demande la liste des SHA256SUM connus
        Postgres -> ServermodelData: envoie la liste des SHA256SUM connus
    end


Entités
-------

.. toctree::
  :maxdepth: 1
  :titlesonly:

  ServerModel <../entities/server-model>
  Release <../entities/release>
  Version <../entities/version>
  Package <../entities/package>
  ModelConfigurationFile <../entities/model-configuration-file>
  ApplicationService <../entities/application-service>


Messages
--------
