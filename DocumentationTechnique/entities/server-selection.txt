ServerSelection
===============

Description
-----------

L'entité ``ServerSelection`` représente une sélection statique (références fixes) ou dynamique (références calculées suivant des critères définis) de serveurs.

Attributs
---------

+----------------------------+-------------------------------------+------------------------------------------------------------------+
| Clé                        | Type                                | Description                                                      |
+============================+=====================================+==================================================================+
| ServerSelectionId          | ID                                  | Clé primaire                                                     |
+----------------------------+-------------------------------------+------------------------------------------------------------------+
| ServerSelectionName        | String                              | Nom associé à la sélection                                       |
+----------------------------+-------------------------------------+------------------------------------------------------------------+
| ServerSelectionDescription | String                              | Description associée à la sélection                              |
+----------------------------+-------------------------------------+------------------------------------------------------------------+
| ServerSelectionServersId   | []ServerId                          | Liste d'id de serveurs (si Dynamique False)                      |
+----------------------------+-------------------------------------+------------------------------------------------------------------+
| ServerSelectionUsers       | hstore(UserName,UserRole)           | Liste d'utilisateurs et de leur profil                           |
+----------------------------+-------------------------------------+------------------------------------------------------------------+
| Dynamique                  | Boolean Default False               | Cette sélection de serveur est-elle dynamique ?                  |
+----------------------------+-------------------------------------+------------------------------------------------------------------+
| Requete                    | String                              | Critère de recherche de la sélection dynamique                   |
+----------------------------+-------------------------------------+------------------------------------------------------------------+

Domaine
-------

- `ServerDomain <../domains/server-domain.html>`_

.. _Server: ./server.html
