Installation et génération de la documentation
==============================================

Les outils pour générer la documentation sont `sphinx doc <http://sphinx-doc.org>`_ et mscgen.

Installation
-------------

Pour générer la documentation les composants suivants doivent être installés :

.. code-block:: bash

    sudo apt install python3-sphinx mscgen python3-jinja2 python3-yaml plantuml
    sudo apt install python3-pip
    sudo pip3 install -U sphinxcontrib-mscgen
    sudo pip3 install -U sphinxcontrib-plantuml

Génération de la documentation en local
----------------------------------------

Se placer dans le répertoire DocumentationTechnique du dépôt Git et exécuter un make :

.. code-block:: bash

    cd DocumentationTechnique
    make html

Consulter la documentation en local :

.. code-block:: bash

    firefox _build/html/index.html

Génération de la documentation sur le serveur
---------------------------------------------

La documentation est générée automatiquement, pour forcer la génération manuellement,
se rendre à l'adresse :

http://jenkins.eole.lan/jenkins/job/Internes/job/zephirdoc/

Se connecter et cliquer sur « Lancer un build… ».

Le résultat est consultable à l'adresse : https://dev-eole.ac-dijon.fr/doc/zephir

Consulter la documentation
---------------------------

La documentation technique est consultable :

- à l'adresse : https://dev-eole.ac-dijon.fr/doc/zephir
- sur l'instance de démonstration : https://lab14-eole.ac-dijon.fr/doc/
- sous forme de conteneur d'aide (--helpers) sur votre instance à l'adresse : `https://<votre_domaine>/doc/ <https://votre_domaine/doc/>`_

La documentation utilisateur (en cours d'écriture) est disponible à l'adresse : http://eole.ac-dijon.fr/documentations/ModuleZephirMicroservices
