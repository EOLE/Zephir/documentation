Stockage de fichiers dans l'application Zéphir
==============================================

La proposition initiale est d'ajouter un service spécifique permettant aux autres micro services d'accéder aux contenu des fichiers (epic 4)

Plusieurs problèmatiques se posent concernant la gestion des fichiers:

* déduplication / réplication des fichiers
* système d'indexation et de recherche dans les fichiers
* Association de méta données métier aux fichiers stockés

Plusieurs approches semblent possibles :

* stockage dans un système de base de données (par exemple, MongoDB avec utilisation de GridFS pour les fichiers volumineux).
* utiliser le système de volume de Docker pour monter les données, et avoir un système qui gère la duplication/réplication à un niveau plus bas (le service de stockage verrait alors un système de fichiers 'classique').
