# Zéphir UI - Maquettes

## Visualiser les maquettes

L'export des maquettes est disponible dans le répertoire `export`. Un simple navigateur Web à jour est nécessaire pour ouvrir le fichier [index.html](./export/index.html).

## Éditer les maquettes

Les maquettes de l'interface graphique Zéphir sont éditées grâce au logiciel [Pencil](http://pencil.evolus.vn/). Une version >= 3.0 est conseillée.

Afin maintenir une cohérence visuelle entre les différentes pages de la maquette, il est conseillé d'installer et utiliser les "stencils" (librairie de composants) [Bootstrap](https://github.com/nathanielw/Bootstrap-Pencil-Stencils) créés par [Nathaniel Watson](https://github.com/nathanielw).
