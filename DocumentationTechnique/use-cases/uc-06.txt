UC-06: Je peux mémoriser mes sélections de serveurs dans des "requêtes enregistrées" pour faciliter l'interaction (dans le profil)
==================================================================================================================================

Prérequis
---------

- L'utilisateur est authentifié sur l'application Zéphir
- Le profil de l'utilisateur a un ou plusieurs rôles lui permettant de voir/sélectionner des serveurs

Domaines liés
-------------

- `UserDomain`_
- `ServerDomain`_

.. _UserDomain: ../domains/user-domain.html
.. _ServerDomain: ../domains/server-domain.html
