UC-08:  Mes "requêtes enregistrées" peuvent être soit statiques, soit dynamiques
================================================================================

Prérequis
---------

- L'utilisateur est authentifié sur l'application Zéphir
- Le profil de l'utilisateur a un ou plusieurs rôles lui permettant de voir/sélectionner des serveurs

Domaines liés
-------------

- `UserDomain`_
- `ServerDomain`_

.. _UserDomain: ../domains/user-domain.html
.. _ServerDomain: ../domains/server-domain.html
