UC-10:  L'utilisateur peut déclencher des actions sur une liste de serveurs
===========================================================================

La liste des actions est :
   - diffuser une configuration (config.eol, modele era, arv, certificats, liste des noms de paquets)
   - lancer un reconfigure
   - reboot du serveur
   - arrêt du serveur
   - mise à jour du serveur
   - gestion de vie d'un service (arrêt/démarrage)
   - exécuter un script/recettte sur le serveur
   - exécuter un script "prédéfini" sur le serveur
   
Prérequis
---------

- L'utilisateur est authentifié sur l'application Zéphir
- Le profil de l'utilisateur a un plusieurs rôles lui permettant de voir/sélectionner/exécuter des actions sur des serveurs


Domaines liés
-------------

- `UserDomain`_
- `AuthorizationDomain`_
- `ServerDomain`_
- `ExecutionDomain`_

.. _UserDomain: ../domains/user-domain.html
.. _AuthorizationDomain: ../domains/authorization-domain.html
.. _ServerDomain: ../domains/server-domain.html
.. _ExecutionDomain: ../domains/execution-domain.html
