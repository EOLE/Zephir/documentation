Développement Zephir-ui en local
==================================

Désactiver le service web
-------------------------

Au moment de lancer Zéphir, ne pas démarrer le service web en utilisant l'argument `--without`

.. code-block:: bash

    zephir-compose -b dev --without web -- up --build -d 


Si une instance Zéphir est déjà en cours d'exécution, arrêter le service web

.. code-block:: bash

   zephir-compose -b dev -- down web

Configurer le service Traefik
------------------------------

Pour faire pointer le service Traefik vers l'interface UI lancée en local
rajouter la configuration ci-dessous dans le fichier :file:`service/treafik/treafik/treafik.toml`.

.. code-block:: bash

    [file]
    [backends]
        [backends.ui]
            [backends.ui.servers.rails]
                url = "http://192.168.230.164:3000" #IP locale du poste de développement
    [frontends]
        [frontends.zephirui]
            entrypoints = ["https"]
            backend = "ui"
            passHostHeader = true
                [frontends.zephirui.routes.all]
                    rule = "Host:zephir2.ac-test.fr" #URL du serveur
 
Redémarrer le service Traefik

.. code-block:: bash

    zephir-compose -b dev -- restart traefik

L'interface UI de Zéphir pointe maintenant vers l'UI lancée en local
