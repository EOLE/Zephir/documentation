# Documentation

Dépot de la documentation technique.

## Consulter la documentation technique

La documentation technique est consultable :

- à l'adresse : [https://dev-eole.ac-dijon.fr/doc/zephir](https://dev-eole.ac-dijon.fr/doc/zephir)
- sur l'instance de démonstration : [https://lab14-eole.ac-dijon.fr/doc/](https://lab14-eole.ac-dijon.fr/doc/)
- sous forme de conteneur d'aide (--helpers) sur votre instance à l'adresse : [https://<votre_domaine>/doc/](https://<votre_domaine>/doc/)

## Consulter la documentation utilisateur

La documentation utilisateur (en cours d'écriture) est disponible à l'adresse : [http://eole.ac-dijon.fr/documentations/ModuleZephirMicroservices](http://eole.ac-dijon.fr/documentations/ModuleZephirMicroservices)

## Génération de la documentation en local

Les outils pour générer la documentation sont [sphinx doc](http://sphinx-doc.org>) et mscgen.

### Installation des outils

Installation de Git :

    sudo apt install git

Pour générer la documentation les composants suivants doivent être installés :

    sudo apt install python3-sphinx mscgen python3-jinja2 python3-yaml plantuml
    sudo apt install python3-pip
    sudo pip3 install -U sphinxcontrib-mscgen
    sudo pip3 install -U sphinxcontrib-plantuml

### Cloner le dépôt de documentation

    git clone https://gitlab.mim.ovh/EOLE/Zephir/documentation.git && cd documentation

### Génération de la documentation

Se placer dans le répertoire DocumentationTechnique du dépôt Git et exécuter un make :

    cd DocumentationTechnique
    make html

Consulter la documentation en local :

    firefox _build/html/index.html

## Génération de la documentation sur le serveur

La documentation est générée automatiquement, pour forcer la génération manuellement,
se rendre à l'adresse :

[http://jenkins.eole.lan/jenkins/job/Internes/job/zephirdoc/](http://jenkins.eole.lan/jenkins/job/Internes/job/zephirdoc/)

Se connecter et cliquer sur « Lancer un build… ».

Le résultat est consultable à l'adresse : [https://dev-eole.ac-dijon.fr/doc/zephir](https://dev-eole.ac-dijon.fr/doc/zephir)
